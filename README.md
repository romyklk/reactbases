**Création d'un Blog simple :**
TP pour pratiquer les concepts de props, state et le passage d'éléments entre composants en React :

**Objectif :** Créez une application de blog simple où les utilisateurs peuvent afficher des articles, ajouter de nouveaux articles et laisser des commentaires.

**Instructions :**

1. Commencez par créer un nouveau projet React si vous n'en avez pas déjà un.

2. Créez un composant `Blog` qui sera la racine de votre application. Dans ce composant, créez un état pour stocker la liste des articles.

3. Créez un composant `ArticleList` pour afficher la liste des articles. Il recevra les articles en tant que props.

4. Créez un composant `Article` pour représenter un article individuel. Ce composant doit accepter des props telles que le titre de l'article, le contenu de l'article et la date de publication.

5. Dans le composant `Blog`, créez un état pour stocker la liste des articles. Initialisez-le avec quelques articles par défaut dans le `constructor`.

6. Affichez la liste des articles en utilisant le composant `Article` dans le composant `ArticleList`. Utilisez une méthode de rendu de liste (par exemple, `map`) pour itérer sur les articles et les passer comme props au composant `Article`.

7. Ajoutez un formulaire dans le composant `Blog` qui permet aux utilisateurs d'ajouter de nouveaux articles. Utilisez l'état pour suivre les nouveaux articles en cours d'ajout et mettez à jour l'état lorsque le formulaire est soumis.

8. Ajoutez un composant `Comment` pour représenter un commentaire. Le composant `Article` peut avoir une liste de commentaires.

9. Permettez aux utilisateurs d'ajouter des commentaires aux articles. Les commentaires devraient être ajoutés à l'article correspondant.

10. Testez votre application en ajoutant de nouveaux articles, en ajoutant des commentaires et en affichant la liste des articles.

Ce TP vous permettra de renforcer votre compréhension des concepts React tels que les props, le state, la gestion des formulaires et la communication entre les composants.




