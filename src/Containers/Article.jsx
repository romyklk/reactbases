/* eslint-disable react/prop-types */

import CommentList from "../Components/CommentList";

export default function Article({ article, onUpdateIsRead }) {
    return (
        <div className="col-md-4">
            <div className="card my-2">
                <img
                    src="https://source.unsplash.com/featured/?books"
                    className="card-img-top"
                    alt="Image de l'article"
                    style={{ height: '200px', objectFit: 'cover' }}
                />
                <div className="card-body">
                    <div className="form-check form-switch">
                        <input
                            className="form-check-input"
                            type="checkbox"
                            id={`flexSwitchCheckChecked-${article.id}`}
                            checked={article.isRead}
                            onChange={() => onUpdateIsRead(article.id)}
                        />
                        <label
                            className="form-check-label"
                            htmlFor={`flexSwitchCheckChecked-${article.id}`}
                        >
                            {article.isRead ? 'Lu' : 'Non lu'}
                        </label>
                    </div>
                    <h5 className="card-title d-flex justify-content-between align-items-center">
                        {article.title}
                    </h5>
                    <p className="card-text">{article.content}</p>
                    {
                        article.isRead ? (
                            <a href="#" className="btn btn-warning disabled">
                                Article lu
                            </a>
                        ) : (
                            <a href="#" className="btn btn-primary">
                                Lire cet article
                            </a>
                        )
                    }



                </div>
                <CommentList comments={article.comments} id={article.id} />
            </div>
        </div>
    );
}
