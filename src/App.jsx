import Header from "./Containers/Header";
import ArticleList from "./Components/ArticleList";
import AddArticle from "./Components/AddArticle";
import { useState } from "react";

let ALL_ARTICLES = [
  {
    id: 1,
    title: "Article 1",
    content: "hello ceci est la description de l'article 1",
    image: "https://source.unsplash.com/featured/?books",
    isRead: true,
    comments: [
      {
        id: 1,
        author: "Léo",
        content: "Commentaire 1",
        articleId: 1
      },
      {
        id: 2,
        author: "Sébastien",
        content: "Commentaire 2",
        articleId: 1
      }
    ]
  },
  {
    id: 2,
    title: "Article 2",
    content: "Dans cet article nous allons voir comment créer une application React.",
    image: "https://source.unsplash.com/featured/?books",
    isRead: false,
    comments: []
  },
  {
    id: 4,
    title: "Article 4",
    content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quos.",
    image: "https://source.unsplash.com/featured/?books",
    isRead: false,
    comments: [
      {
        id: 7,
        author: "Marie",
        content: "Commentaire 7",
        articleId: 4
      },
      {
        id: 8,
        author: "Philippe",
        content: "Commentaire 8",
        articleId: 4
      }
    ]
  }
];

function App() {
  const [allArticles, setAllArticles] = useState(ALL_ARTICLES);
  const [showOrHideForm, setShowOrHideForm] = useState(false);

  const addArticle = (newArticle) => {
    setAllArticles([...allArticles, newArticle]);
  };

  const updateIsRead = (articleId) => {
    setAllArticles((articles) =>
      articles.map((article) =>
        article.id === articleId
          ? { ...article, isRead: !article.isRead }
          : article
      )
    );
  };

  return (
    <>
      <Header />

      <main>
        <section className="container">
          <button
            className={`btn ${showOrHideForm ? "btn-danger" : "btn-success"} my-2`}
            onClick={() => setShowOrHideForm(!showOrHideForm)}
          >
            {showOrHideForm ? "Cacher le formulaire" : "Ajouter un article"}
          </button>
          {showOrHideForm && <AddArticle onAddArticle={addArticle} onUpdateIsRead={updateIsRead} />}
        </section>

        <hr />
        <section className="container">
          <h1 className="text-center my-4">
            Liste des {allArticles.length} articles de la bibliothèque
          </h1>
          <ArticleList articles={allArticles} onUpdateIsRead={updateIsRead} />
        </section>
      </main>
    </>
  );
}

export default App;
