/* eslint-disable react/prop-types */

import Article from "../Containers/Article";

export default function ArticleList({ articles, onUpdateIsRead }) {

    const rows = articles.map((article) => (
        <Article key={article.id}
            article={article}
            onUpdateIsRead={onUpdateIsRead}
        />
    ));

    return <div className="row mt-4">{rows}</div>;
}
