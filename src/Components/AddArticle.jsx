/* eslint-disable react/prop-types */

import { useForm } from "react-hook-form";
import { nanoid } from "nanoid";


export default function AddArticle(props) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();

    let newArticle = {
        id: nanoid(6),
        title: "",
        content: "",
        image: "",
        isRead: false,
        comments: []
    };

    const onSubmit = (data) => {
        const monImage = data.image ? data.image : "https://source.unsplash.com/featured/?books";

        newArticle = {
            id: nanoid(6),
            title: data.title,
            content: data.content,
            image: monImage,
            isRead: false,
            comments: [],
        };

        props.onAddArticle(newArticle);
        reset();

    }

    return (
        <div className="container mt-4 w-50 mx-auto">
            <h1>Ajouter un article</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group">
                    <label htmlFor="title">Titre</label>
                    <input
                        type="text"
                        className="form-control"
                        autoComplete="none"
                        {...register("title", { required: true, minLength: 3, maxLength: 100 })}
                    />
                    {errors.title && <span className="text-danger">Le titre est obligatoire et doit contenir entre 3 et 100 caractères</span>}
                </div>
                <div className="form-group">
                    <label htmlFor="content">Contenu</label>
                    <textarea
                        className="form-control"
                        rows="3"
                        autoComplete="none"
                        {...register("content", { required: true, minLength: 10 })}
                    ></textarea>
                    {errors.content && <span className="text-danger">Le contenu est obligatoire et doit contenir au moins 10 caractères</span>}
                </div>
                <div className="form-group">
                    <label htmlFor="image">Image</label>
                    <input
                        type="url"
                        className="form-control"
                        autoComplete="none"
                        {...register("image", { required: false })}
                    />
                </div>
                <button type="submit" className="btn btn-primary mt-2">Ajouter</button>
            </form>
        </div>
    );
}
