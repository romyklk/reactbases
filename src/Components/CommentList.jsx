/* eslint-disable react/prop-types */

import { useForm } from 'react-hook-form';
import Comment from '../Containers/Comment';
import { nanoid } from 'nanoid';

export default function CommentList({ comments, id }) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();

    const onSubmit = (data) => {
        const newComment = {
            id: nanoid(),
            author: data.author,
            content: data.comment,
        };

        comments.push(newComment);

        reset();
    };

    const renderComments = (comments) => {
        return (
            <div>
                {comments.map((comment) => (
                    <Comment key={comment.id} content={comment.content} author={comment.author} />
                ))}
            </div>
        );
    };

    return (
        <div className="card-footer">
            <button className={`btn ${comments.length === 0 ? 'btn-danger' : 'btn-info'} my-2`} data-bs-toggle="collapse" data-bs-target={`#commentsCollapse-${id}`}>
                {comments.length === 0 ? '+' : `Voir${comments.length > 1 ? ' les ' + comments.length : ' le '} commentaire${comments.length > 1 ? 's' : ''} de l'article`}
            </button>
            <div id={`commentsCollapse-${id}`} className="collapse">
                {comments.length > 0 && renderComments(comments)}

                <form onSubmit={handleSubmit(onSubmit)} style={{ backgroundColor: '#acb6ec' }} className="p-3 rounded">
                    <div className="form-group mb-2">
                        <input
                            type="text"
                            className="form-control"
                            id="author"
                            placeholder="Votre nom"
                            {...register('author', { required: true })}
                        />
                        {errors.author && <span className="text-danger">Le nom est obligatoire</span>}
                    </div>
                    <div className="form-group">
                        <textarea
                            className="form-control"
                            id="comment"
                            rows="3"
                            placeholder="Votre commentaire"
                            {...register('comment', { required: true, minLength: 10 })}
                        ></textarea>
                        {errors.comment && <span className="text-danger">Le contenu est obligatoire et doit contenir au moins 10 caractères</span>}
                    </div>
                    <button type="submit" className="btn btn-warning mt-2">
                        Envoyer
                    </button>
                </form>
            </div>
        </div>
    );
}
